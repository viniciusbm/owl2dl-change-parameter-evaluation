#!/usr/bin/env python3

from collections import OrderedDict
from itertools import product
from pathlib import Path

import os
import sys
import sqlite3

test_case_dir = sys.argv[1]
db_file = sys.argv[2]
replications = 10
tests = 3



parameters = OrderedDict([
    ('operation', ['kernel', 'remainder']),
    ('esw',       ['1%', '25%', '50%', '75%', '100%', '10']),
    ('ssw',       ['1%', '25%', '50%', '75%', '100%', '10']),
    ('c',         ['none', 'binary', 'count']),
    ('edc',       [False, True]),
    ('sdc',       [False, True]),
])


conn = sqlite3.connect(db_file)
c = conn.cursor()

c.execute('''
    CREATE TABLE execution (
        id INT PRIMARY KEY,
        ontology TEXT,
        operation TEXT,
        sentence TEXT,
        esw TEXT,
        ssw TEXT,
        c TEXT,
        edc TEXT,
        sdc TEXT,
        replication INT,
        status TEXT DEFAULT "pending",
        measured_values TEXT DEFAULT "",
    );
    ''');



for f in os.scandir(test_case_dir):
    if not f.path.endswith('.finished'):
        continue
    f = os.path.abspath(f.path)[:-9]
    path = Path(f + '.output')
    if path.is_file() and 'Success!' in path.read_text():
        for t in range(1, tests+1):
            ontology = f'{f}_{t}_onto.owl.xml'
            sentence = f'{f}_test_{t}_pos.owl.xml'
            for comb in product(*parameters.values()):
                for replication in range(1, replications + 1):
                    c.execute(f'''
                        INSERT INTO execution
                        (ontology, sentence, {' ,'.join(parameters.keys())})
                        VALUES ({', '.join((len(parameters) + 2 ) * ['?'])})
                    ''', (ontology, sentence, *comb))
conn.commit()
conn.close()
