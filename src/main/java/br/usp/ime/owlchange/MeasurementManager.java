/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.IntSummaryStatistics;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.infra.IterationParams;
import org.openjdk.jmh.profile.InternalProfiler;
import org.openjdk.jmh.results.AggregationPolicy;
import org.openjdk.jmh.results.IterationResult;
import org.openjdk.jmh.results.Result;
import org.openjdk.jmh.results.ScalarResult;
import org.semanticweb.owlapi.model.OWLAxiom;

import com.google.common.collect.Lists;

public class MeasurementManager implements InternalProfiler {

    private static long start;
    private static final Map<String, Result<?>> results = new ConcurrentHashMap<>();
    private static Set<Set<OWLAxiom>> returnValue;

    public static void reset() {
        results.clear();
        start = System.nanoTime();
    }

    public static void setReturnValue(Set<Set<OWLAxiom>> returnValue) {
        MeasurementManager.returnValue = returnValue;
    }

    public static void tag(String prefix) {
        setTime(prefix);
        setVmHWM(prefix);
    }

    public static void setResult(String prefix, double value, String unit) {
        ScalarResult result = new ScalarResult(prefix, value, unit,
                AggregationPolicy.AVG);
        results.put(result.getLabel(), result);
    }

    private static void setTime(String prefix) {
        ScalarResult result = new ScalarResult(prefix + ".time",
                System.nanoTime() - start, "ns", AggregationPolicy.AVG);
        results.putIfAbsent(result.getLabel(), result);
    }

    private static void setVmHWM(String prefix) {
        Path path = Paths.get("/proc/self/status");
        try (BufferedReader reader = Files.newBufferedReader(path,
                StandardCharsets.UTF_8)) {
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {
                if (currentLine.startsWith("VmHWM")) {
                    String[] tokens = currentLine.split("\\s+");
                    if (tokens.length < 3) {
                        // log invalid format
                        return;
                    }
                    ScalarResult result = new ScalarResult(prefix + ".vmhwm",
                            Double.parseDouble(tokens[1]), tokens[2],
                            AggregationPolicy.AVG);
                    results.putIfAbsent(result.getLabel(), result);
                }
            }
        } catch (IOException ex) {
            // log exception
            return;
        }
    }

    @Override
    public void beforeIteration(BenchmarkParams benchmarkParams,
            IterationParams iterationParams) {
        MeasurementManager.reset();
        setResult("tstart", start, "ns");
    }

    @Override
    public Collection<? extends Result> afterIteration(
            BenchmarkParams benchmarkParams, IterationParams iterationParams,
            IterationResult result) {
        if (returnValue != null) {
            IntSummaryStatistics summary = returnValue.stream()
                    .collect(Collectors.summarizingInt(Set::size));

            MeasurementManager.setResult("avgsize", summary.getAverage(), "x");
            MeasurementManager.setResult("minsize", summary.getMin(), "x");
            MeasurementManager.setResult("maxsize", summary.getMax(), "x");
            MeasurementManager.setResult("count", returnValue.size(), "x");
        }
        return Lists.newArrayList(results.values());
    }

    @Override
    public String getDescription() {
        return "My Profiler";
    }

    public static Map<String, Result<?>> getResults() {
        return Collections.unmodifiableMap(results);
    }
}
