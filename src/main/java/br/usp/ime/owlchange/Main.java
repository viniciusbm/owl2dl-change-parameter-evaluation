/*
 * Copyright 2018-2019 OWL2DL-Change Developers
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package br.usp.ime.owlchange;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.expression.OWLEntityChecker;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;

import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.HittingSetMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.BlackBoxMaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.DivideAndConquerMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.MaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.RelativeSizeSlidingWindowMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.DivideAndConquerMaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.MaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.RelativeSizeSlidingWindowMaxNonShrinker;
import br.usp.ime.owlchange.minimp.full.HittingSetMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.BlackBoxMinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.DivideAndConquerMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.MinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.RelativeSizeSlidingWindowMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.DivideAndConquerMinImpShrinker;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.MinImpShrinker;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.RelativeSizeSlidingWindowMinImpShrinker;
import br.usp.ime.owlchange.util.SentenceFormatter;
import br.usp.ime.owlchange.util.SyntacticConnectivitySorting;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class Main {

    enum Operation {
        MinImp, MaxNon
    }

    private static RepairResult<OWLAxiom> buildMaxNons(MaxNonsBuilder builder,
            Set<OWLAxiom> ontologyAxioms,
            GeneralisedPackageEntailmentChecker checker) {
        MeasurementManager.reset();
        MeasurementManager.tag("START");
        RepairResult<OWLAxiom> result = builder.maxNons(ontologyAxioms,
                checker);
        MeasurementManager.tag("END");
        return result;
    }

    private static RepairResult<OWLAxiom> buildMinImps(MinImpsBuilder builder,
            Set<OWLAxiom> ontologyAxioms,
            GeneralisedPackageEntailmentChecker checker) {
        MeasurementManager.reset();
        MeasurementManager.tag("START");
        RepairResult<OWLAxiom> result = builder.minImps(ontologyAxioms,
                checker);
        MeasurementManager.tag("END");
        return result;
    }

    public static void main(String[] args) {
        String ontologyFileName, operationStr, alphaStr;
        File ontologyFile;
        Operation operation = null;
        OWLAxiom alpha;
        float ssw, esw;
        boolean sdc, edc;
        String syntacticConnectivityOrder;
        ArgumentParser parser = ArgumentParsers.newFor("prog").build();
        parser.addArgument("ontology").type(String.class)
                .help("the ontology O");
        parser.addArgument("operation")
                .help("the type of operation to compute "
                        + "(remainder = MaxNon = maximal non-implying subset; "
                        + "kernel = MinImp = minimal implying subset)")
                .choices("MinImp", "kernel", "MaxNon", "remainder");
        parser.addArgument("--syntactic-connectivity", "-c").help(
                "the order in which the sentences will be processed in the first phase"
                        + " (shrinking for MaxNon/remainder and enlarging for MinImp/kernel)"
                        + " according to syntactic connectivity with the sentence")
                .choices("none", "binary", "count").setDefault("none");
        parser.addArgument("sentence").type(String.class)
                .help("the sentence α");
        parser.addArgument("--shrink-sliding-window", "-ssw").setDefault(0f)
                .help("relative size of the sliding window for the shrinking phase"
                        + " (ignored if this phase uses divide-and-conquer strategy)")
                .type(Float.class);
        parser.addArgument("--enlarge-sliding-window", "-esw").setDefault(0f)
                .help("relative size of the sliding window for the enlarging phase"
                        + " (ignored if this phase uses divide-and-conquer strategy)")
                .type(Float.class);
        parser.addArgument("--enlarge-divide-and-conquer", "-edc")
                .action(Arguments.storeTrue())
                .help("uses divide-and-conquer algorithm for enlarging");
        parser.addArgument("--shrink-divide-and-conquer", "-sdc")
                .action(Arguments.storeTrue())
                .help("uses divide-and-conquer algorithm for shrinking");
        try {
            Namespace res = parser.parseArgs(args);
            System.out.println(res);
            ontologyFileName = res.getString("ontology");
            operationStr = res.getString("operation").toLowerCase().trim();
            alphaStr = res.getString("sentence");
            ssw = res.getFloat("shrink_sliding_window");
            esw = res.getFloat("enlarge_sliding_window");
            sdc = res.getBoolean("shrink_divide_and_conquer");
            edc = res.getBoolean("enlarge_divide_and_conquer");
            syntacticConnectivityOrder = res
                    .getString("syntactic_connectivity");
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(-1);
            return; // only to avoid warnings about variable initialisation
        }
        // read ontology
        ontologyFile = new File(ontologyFileName);
        if (!ontologyFile.isFile()) {
            System.err.println("Ontology file not found.");
        }
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = null;
        try {
            ontology = manager.loadOntologyFromOntologyDocument(ontologyFile);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        // parse operation
        switch (operationStr) {
        case "minimp":
        case "kernel":
            operation = Operation.MinImp;
            break;
        case "maxnon":
        case "remainder":
            operation = Operation.MaxNon;
            break;
        default:
            System.err.println("Invalid operation.");
            System.exit(-1);
        }
        // parse sentence
        File sentenceFile = new File(alphaStr);
        if (sentenceFile.isFile()) {
            // the sentence is stored as an ontology with a single
            // non-declaration sentence
            OWLOntology sentenceOnto = null;
            try {
                sentenceOnto = manager
                        .loadOntologyFromOntologyDocument(sentenceFile);
            } catch (OWLOntologyCreationException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            alpha = sentenceOnto.axioms()
                    .filter(a -> !a.isOfType(AxiomType.DECLARATION)).findFirst()
                    .get();
        } else {
            // the sentence is in the string itself, in Manchester syntax
            OWLEntityChecker entityChecker = new ShortFormEntityChecker(
                    new BidirectionalShortFormProviderAdapter(manager,
                            ontology.importsClosure()
                                    .collect(Collectors.toSet()),
                            new SimpleShortFormProvider()));
            ManchesterOWLSyntaxParser sentenceParser = OWLManager
                    .createManchesterParser();
            sentenceParser.setDefaultOntology(ontology);
            sentenceParser.setOWLEntityChecker(entityChecker);
            sentenceParser.setStringToParse(alphaStr);
            alpha = sentenceParser.parseAxiom();
        }
        // perform operation
        OWLReasonerFactory reasonerFactory = new ReasonerFactory();
        Set<OWLAxiom> entail = new HashSet<>();
        entail.add(alpha);
        GeneralisedPackageEntailmentChecker checker = null;
        try {
            checker = new GeneralisedPackageEntailmentChecker(reasonerFactory,
                    null, entail);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        Set<OWLAxiom> ontologyAxioms = ontology.axioms()
                .collect(Collectors.toSet());
        System.out.println("Ontology:  " + ontology.getOntologyID() + "");
        System.out.println("Operation: " + operation);
        System.out.println(
                "Sentence:  " + SentenceFormatter.humanReadable(alpha) + "");
        System.out.println("The ontology "
                + (checker.hasProperty(ontologyAxioms) ? "entails"
                        : "does not entail")
                + " the sentence.");
        RepairResult<OWLAxiom> result = null;
        if (syntacticConnectivityOrder != "none") {
            ontologyAxioms = new SyntacticConnectivitySorting(
                    alpha.signature().collect(Collectors.toSet()),
                    SyntacticConnectivitySorting.Order
                            .valueOf(syntacticConnectivityOrder.toUpperCase()))
                                    .sort(ontologyAxioms);
        }
        System.out.println(SentenceFormatter
                .humanReadableSetOfSentences(ontologyAxioms, true));
        MeasurementManager.reset();
        switch (operation) {
        case MaxNon:
            MaxNonShrinker maxNonShrinker = sdc
                    ? new DivideAndConquerMaxNonShrinker()
                    : new RelativeSizeSlidingWindowMaxNonShrinker(ssw);
            MaxNonEnlarger maxNonEnlarger = edc
                    ? new DivideAndConquerMaxNonEnlarger()
                    : new RelativeSizeSlidingWindowMaxNonEnlarger(esw);
            MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
                    maxNonShrinker, maxNonEnlarger);
            MaxNonsBuilder maxNonsBuilder = new HittingSetMaxNonsBuilder(
                    maxNonBuilder, HSQueue::new);
            result = buildMaxNons(maxNonsBuilder, ontologyAxioms, checker);
            break;
        case MinImp:
            MinImpEnlarger minImpEnlarger = edc
                    ? new DivideAndConquerMinImpEnlarger()
                    : new RelativeSizeSlidingWindowMinImpEnlarger(esw);
            MinImpShrinker minImpShrinker = sdc
                    ? new DivideAndConquerMinImpShrinker()
                    : new RelativeSizeSlidingWindowMinImpShrinker(ssw);
            MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(
                    minImpEnlarger, minImpShrinker);
            MinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(
                    minImpBuilder, HSQueue::new);
            result = buildMinImps(minImpsBuilder, ontologyAxioms, checker);
            break;
        default:
            break;
        }
        MeasurementManager.setReturnValue(result.getNodes());
        MeasurementManager.setResult("CALLS", checker.getCalls(), "x");
        System.out.println("Result:\n" + SentenceFormatter
                .humanReadableSetOfSetsOfSentences(result.getNodes(), true));
        System.out
                .println("MEASURED VALUES: " + MeasurementManager.getResults());
    }
}
