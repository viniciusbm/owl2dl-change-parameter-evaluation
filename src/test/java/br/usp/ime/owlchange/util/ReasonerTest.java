/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.util;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.stream.Stream;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import uk.ac.manchester.cs.jfact.JFactFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLDeclarationAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLSubObjectPropertyOfAxiomImpl;

/* TODO: send this as a JFaCT Issue */
public class ReasonerTest {

  static Stream<OWLReasonerFactory> reasonerFactoryProvider() {
    return Stream.of(new ReasonerFactory(), new JFactFactory());
  }

  // JFaCT Fails this one
  @Disabled
  @ParameterizedTest
  @MethodSource("reasonerFactoryProvider")
  void emptyEntailsFreshSubTopRole(OWLReasonerFactory reasonerFactory)
      throws OWLOntologyCreationException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology emptyOntology = manager.createOntology();
    OWLReasoner reasoner = reasonerFactory.createNonBufferingReasoner(emptyOntology);
    OWLDataFactory df = manager.getOWLDataFactory();
    PrefixManager prefixManager = new DefaultPrefixManager("http://test.org/onto");
    OWLObjectProperty fresh = df.getOWLObjectProperty("#r", prefixManager);
    OWLSubObjectPropertyOfAxiomImpl entailment = new OWLSubObjectPropertyOfAxiomImpl(
        fresh, df.getOWLTopObjectProperty(), Collections.emptySet());

    assertTrue(reasoner.isEntailed(entailment));
  }

  @ParameterizedTest
  @MethodSource("reasonerFactoryProvider")
  void emptyEntailsFreshSubTopRoleAfterAssertion(OWLReasonerFactory reasonerFactory)
      throws OWLOntologyCreationException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology emptyOntology = manager.createOntology();
    OWLReasoner reasoner = reasonerFactory.createNonBufferingReasoner(emptyOntology);
    OWLDataFactory df = manager.getOWLDataFactory();
    PrefixManager prefixManager = new DefaultPrefixManager("http://test.org/onto");
    OWLObjectProperty fresh = df.getOWLObjectProperty("#r", prefixManager);
    OWLDeclarationAxiomImpl declaration = new OWLDeclarationAxiomImpl(fresh,
        Collections.emptySet());
    emptyOntology.addAxiom(declaration);
    OWLSubObjectPropertyOfAxiomImpl entailment = new OWLSubObjectPropertyOfAxiomImpl(
        fresh, df.getOWLTopObjectProperty(), Collections.emptySet());

    assertTrue(reasoner.isEntailed(entailment));
  }

}
