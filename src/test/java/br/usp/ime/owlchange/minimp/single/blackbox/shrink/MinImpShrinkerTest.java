/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.single.blackbox.shrink;

import static org.junit.jupiter.api.Assertions.assertFalse;

import br.usp.ime.owlchange.GeneralisedPackageEntailmentChecker;
import br.usp.ime.owlchange.minimp.single.MinImpValidator;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public abstract class MinImpShrinkerTest {

  final static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  @Test
  abstract void producesValidMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  void producesValidMinImps(MinImpShrinker minImpShrinker, Set<OWLAxiom> ontology,
      Set<OWLAxiom> entailments) throws OWLOntologyCreationException {
    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections.emptySet(), entailments);

    Optional<Set<OWLAxiom>> optShrinked = minImpShrinker
        .shrink(ontology, checker);

    if (optShrinked.isPresent()) {
      Set<OWLAxiom> candidate = optShrinked.get();
      Assertions.assertTrue(MinImpValidator.isMinImp(candidate, ontology, checker));
    } else {
      assertFalse(checker.hasProperty(ontology));
    }
  }
}
