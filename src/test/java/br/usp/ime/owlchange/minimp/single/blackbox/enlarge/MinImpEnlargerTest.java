/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.single.blackbox.enlarge;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import br.usp.ime.owlchange.GeneralisedPackageEntailmentChecker;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.minimp.single.MinImpValidator;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public abstract class MinImpEnlargerTest {

  final static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  @Test
  abstract void allEmptyReturnEmpty();

  void allEmptyReturnEmpty(MinImpEnlarger minImpEnlarger) {
    OntologyPropertyChecker mockedChecker = mock(OntologyPropertyChecker.class);
    when(mockedChecker.hasProperty(any())).thenReturn(true);
    Set<OWLAxiom> emptyOntology = Sets.newHashSet();

    Set<OWLAxiom> enlarged = minImpEnlarger.enlarge(emptyOntology, mockedChecker)
        .orElseThrow(NoSuchElementException::new);
    assertTrue(enlarged.isEmpty());
  }

  @Test
  abstract void emptyOntologyInconsistentFormulaReturnsNull();

  void emptyOntologyInconsistentFormulaReturnsNull(MinImpEnlarger minImpEnlarger) {
    OntologyPropertyChecker mockedChecker = mock(OntologyPropertyChecker.class);
    when(mockedChecker.hasProperty(any())).thenReturn(false);

    Set<OWLAxiom> emptyOntology = Sets.newHashSet();

    Optional<Set<OWLAxiom>> optEnlarged = minImpEnlarger.enlarge(emptyOntology, mockedChecker);
    assertFalse(optEnlarged.isPresent());
  }

  @Test
  abstract void producesEntailingSubsets(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  void producesEntailingSubsets(MinImpEnlarger minImpEnlarger, Set<OWLAxiom> ontology,
      Set<OWLAxiom> entailments) throws OWLOntologyCreationException {

    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections.emptySet(), entailments);

    Optional<Set<OWLAxiom>> optEnlarged = minImpEnlarger.enlarge(ontology, checker);

    if (optEnlarged.isPresent()) {
      Set<OWLAxiom> enlarged = optEnlarged.get();
      Assertions.assertTrue(MinImpValidator.isSubset(enlarged, ontology));
      assertTrue(MinImpValidator.isImplicant(enlarged, ontology, checker));
    } else {
      assertFalse(checker.hasProperty(ontology));
    }
  }
}
