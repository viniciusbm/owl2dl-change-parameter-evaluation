/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.full;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import br.usp.ime.owlchange.GeneralisedPackageEntailmentChecker;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.TestCaseUtils;
import br.usp.ime.owlchange.minimp.single.MinImpValidator;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLRuntimeException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;

public abstract class MinImpsBuilderTest {

  public final static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  @Test
  protected abstract void emptyOntologyNoMinImpForTautology() throws OWLOntologyCreationException;

  protected void emptyOntologyNoMinImpForTautology(MinImpsBuilder minImpBuilder) {

    Set<OWLAxiom> ontology = Sets.newHashSet();

    OntologyPropertyChecker mockedChecker = mock(OntologyPropertyChecker.class);
    when(mockedChecker.hasProperty(any())).thenReturn(true);

    Set<Set<OWLAxiom>> minImps = minImpBuilder.minImps(ontology, mockedChecker).getNodes();

    assertEquals(0, minImps.size());
  }

  @Test
  protected abstract void elementsAreMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  protected void elementsAreMinImps(MinImpsBuilder minImpBuilder, Set<OWLAxiom> ontology,
      Set<OWLAxiom> entailments) throws OWLOntologyCreationException {

    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections
        .emptySet(), entailments);

    Set<Set<OWLAxiom>> minImps = minImpBuilder.minImps(ontology, checker).getNodes();

    for (Set<OWLAxiom> minImp : minImps) {
      assertTrue(MinImpValidator.isMinImp(ImmutableSet.copyOf(minImp), ontology, checker));
    }
  }

  @Test
  protected abstract void completeMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  void completeMinImps(MinImpsBuilder minImpBuilder, Set<OWLAxiom> ontology,
      Set<OWLAxiom> entailments) throws OWLOntologyCreationException {
    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections
        .emptySet(), entailments);

    Set<Set<OWLAxiom>> minImps = minImpBuilder.minImps(ontology, checker).getNodes();

    assertTrue(MinImpsSanitiser.sanitise(minImps, ontology, checker));
  }

  @Test
  protected abstract void doesNotModifyInputOntology() throws OWLOntologyCreationException;

  void doesNotModifyInputOntology(MinImpsBuilder minImpBuilder)
      throws OWLOntologyCreationException {

    OWLOntology rawOntology = TestCaseUtils.loadOntology(manager, "accounts.owl");

    OWLDataFactory dataFactory = manager.getOWLDataFactory();

    IRI ontologyIRI = rawOntology.getOntologyID().getOntologyIRI()
        .orElseThrow(OWLRuntimeException::new);
    OWLClass accountClass = dataFactory.getOWLClass(ontologyIRI.getIRIString() + "#Account");
    OWLClass storeAccountClass = dataFactory
        .getOWLClass(ontologyIRI.getIRIString() + "#StoreAccount");

    OWLSubClassOfAxiom trigger = new OWLSubClassOfAxiomImpl(storeAccountClass, accountClass,
        newHashSet());

    Set<OWLAxiom> ontology = rawOntology.axioms().collect(Collectors.toSet());

    Set<OWLAxiom> copyOntology = Sets.newHashSet(ontology);
    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections.emptySet(), Sets.newHashSet(trigger));

    minImpBuilder.minImps(ontology, checker);

    assertEquals(copyOntology, rawOntology.axioms().collect(Collectors.toSet()));
  }
}
