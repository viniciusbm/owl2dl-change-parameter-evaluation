/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.single;

import static org.junit.jupiter.api.Assertions.assertTrue;

import br.usp.ime.owlchange.GeneralisedPackageEntailmentChecker;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public abstract class MaxNonBuilderTest {

  protected static final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  @Test
  public abstract void producesMaxNons(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  public void producesMaxNons(MaxNonBuilder maxNonBuilder,
      Set<OWLAxiom> ontology, Set<OWLAxiom> entailments) throws OWLOntologyCreationException {

    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections
        .emptySet(), entailments);

    Optional<Set<OWLAxiom>> optMaxNon = maxNonBuilder
        .maxNon(ontology, checker, Collections.emptySet());

    if (optMaxNon.isPresent()) {
      Set<OWLAxiom> candidate = optMaxNon.get();
      assertTrue(MaxNonValidator.isMaxNon(candidate, ontology, checker));
    } else {
      assertTrue(checker.hasProperty(Sets.newHashSet()));
    }
  }
}
