/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.full;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import br.usp.ime.owlchange.GeneralisedPackageEntailmentChecker;
import br.usp.ime.owlchange.TestCaseUtils;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLRuntimeException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;

public abstract class MaxNonsBuilderTest {

  public final static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

  @Test
  public abstract void elementsAreMaxNons(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException;

  protected void elementsAreMaxNons(MaxNonsBuilder maxNonsBuilder, Set<OWLAxiom> ontology,
      Set<OWLAxiom> entailments) throws OWLOntologyCreationException {

    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections.emptySet(), entailments);

    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(ontology, checker).getNodes();

    assertTrue(MaxNonsSanitiser.sanitise(maxNons, ontology, checker));
  }

  @Test
  protected abstract void doesNotModifyInputOntology() throws OWLOntologyCreationException;

  void doesNotModifyInputOntology(MaxNonsBuilder maxNonsBuilder)
      throws OWLOntologyCreationException {

    OWLOntology rawOntology = TestCaseUtils.loadOntology(manager, "accounts.owl");

    Set<OWLAxiom> ontology = rawOntology.axioms().collect(Collectors.toSet());

    OWLDataFactory dataFactory = manager.getOWLDataFactory();

    IRI ontologyIRI = rawOntology.getOntologyID().getOntologyIRI()
        .orElseThrow(OWLRuntimeException::new);
    OWLClass accountClass = dataFactory.getOWLClass(ontologyIRI.getIRIString() + "#Account");
    OWLClass storeAccountClass = dataFactory
        .getOWLClass(ontologyIRI.getIRIString() + "#StoreAccount");

    OWLSubClassOfAxiom trigger = new OWLSubClassOfAxiomImpl(storeAccountClass, accountClass,
        newHashSet());

    Set<OWLAxiom> copyOntology = Sets.newHashSet(ontology);

    GeneralisedPackageEntailmentChecker checker = new GeneralisedPackageEntailmentChecker(
        new ReasonerFactory(), Collections.emptySet(), Sets.newHashSet(trigger));

    maxNonsBuilder.maxNons(ontology, checker);

    assertEquals(copyOntology, ontology);
  }
}
