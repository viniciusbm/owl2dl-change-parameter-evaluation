/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.single.blackbox.enlarge;

import br.usp.ime.owlchange.TestCaseUtils;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class SlidingWindowMaxNonEnlargerTest extends MaxNonEnlargerTest {

  private static Iterable<Arguments> ontologyAnySubsumptionAndWindowSizeProvider() {

    List<Arguments> arguments = new ArrayList<>(10);

    OWLOntology ontology = TestCaseUtils
        .loadOntology(MaxNonEnlargerTest.manager, "accounts.owl");
    for (int windowSize = 2; windowSize < 4; windowSize += 2) {
      arguments.add(
          Arguments.of(ontology.axioms().collect(Collectors.toSet()),
              Sets.newHashSet(TestCaseUtils.getAnySubsumption(ontology)),
              windowSize));
    }

    return arguments;
  }

  @ParameterizedTest
  @MethodSource("ontologyAnySubsumptionAndWindowSizeProvider")
  void producesValidMaxNons(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments, int windowSize)
      throws OWLOntologyCreationException {
    MaxNonEnlarger maxNonEnlarger = new SlidingWindowMaxNonEnlarger(windowSize);
    producesValidMaxNons(maxNonEnlarger, ontology, entailments);
  }

  @Override
  @Test
  @Disabled
  void producesValidMaxNons(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments) {

  }
}
